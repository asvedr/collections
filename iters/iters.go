package iters

import "iter"

func Chain[T any](iters ...iter.Seq[T]) iter.Seq[T] {
	return func(yield func(T) bool) {
		for _, iter := range iters {
			iter(yield)
		}
	}
}

func Take[T any](iter iter.Seq[T], cnt int) iter.Seq[T] {
	return func(yield func(T) bool) {
		i := 0
		for item := range iter {
			if i >= cnt {
				return
			}
			if !yield(item) {
				return
			}
			i++
		}
	}
}

func Skip[T any](iter iter.Seq[T], cnt int) iter.Seq[T] {
	return func(yield func(T) bool) {
		i := 0
		for item := range iter {
			if i < cnt {
				i++
				continue
			}
			if !yield(item) {
				return
			}
		}
	}
}

func Keys[K, V any](iter iter.Seq2[K, V]) iter.Seq[K] {
	return func(yield func(K) bool) {
		iter(func(x K, _ V) bool {
			return yield(x)
		})
	}
}

func Values[K, V any](iter iter.Seq2[K, V]) iter.Seq[V] {
	return func(yield func(V) bool) {
		iter(func(_ K, x V) bool {
			return yield(x)
		})
	}
}

func Foreach[T any](
	f func(T) error,
	iter iter.Seq[T],
) error {
	for item := range iter {
		if err := f(item); err != nil {
			return err
		}
	}
	return nil
}

func Foreach2[A, B any](
	f func(A, B) error,
	iter iter.Seq2[A, B],
) error {
	for x, y := range iter {
		if err := f(x, y); err != nil {
			return err
		}
	}
	return nil
}
