package iters_test

import (
	"errors"
	"slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/avl"
	"gitlab.com/asvedr/collections/iters"
)

func TestChain(t *testing.T) {
	iter := iters.Chain(
		slices.Values([]int{1, 2}),
		slices.Values([]int{3, 4}),
		slices.Values([]int{5}),
	)
	assert.Equal(
		t,
		[]int{1, 2, 3, 4, 5},
		slices.Collect(iter),
	)
}

func TestSkipNTake(t *testing.T) {
	src := []int{1, 2, 3, 4, 5, 6}
	iter := iters.Skip(slices.Values(src), 2)
	iter = iters.Take(iter, 3)
	assert.Equal(
		t,
		[]int{3, 4, 5},
		slices.Collect(iter),
	)
}

func TestKeys(t *testing.T) {
	src := []string{"a", "b", "c"}
	assert.Equal(
		t,
		[]int{0, 1, 2},
		slices.Collect(iters.Keys(slices.All(src))),
	)
	assert.Equal(
		t,
		[]string{"a", "b", "c"},
		slices.Collect(iters.Values(slices.All(src))),
	)
}

func TestForeach(t *testing.T) {
	var out []int
	f := func(x int) error {
		if x > 2 {
			return errors.New("err")
		}
		out = append(out, x)
		return nil
	}

	err := iters.Foreach(f, slices.Values([]int{1, 2, 3, 4}))
	assert.Equal(t, []int{1, 2}, out)
	assert.Equal(t, "err", err.Error())

	out = []int{}
	err = iters.Foreach(f, slices.Values([]int{1, 2}))
	assert.Equal(t, []int{1, 2}, out)
	assert.NoError(t, err)
}

func TestForeach2(t *testing.T) {
	var out []string
	tree := avl.Map[string, int]{}.Add("a", 1).Add("b", 2).Add("c", 3)
	f := func(x string, y int) error {
		if y > 2 {
			return errors.New("err")
		}
		out = append(out, x)
		return nil
	}
	err := iters.Foreach2(f, tree.Iter)
	assert.Equal(t, []string{"a", "b"}, out)
	assert.Equal(t, "err", err.Error())

	tree = avl.Map[string, int]{}.Add("a", 1).Add("b", 2)
	out = []string{}
	err = iters.Foreach2(f, tree.Iter)
	assert.Equal(t, []string{"a", "b"}, out)
	assert.NoError(t, err)
}
