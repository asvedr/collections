package errors_test

import (
	stderr "errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/errors"
)

type EVal struct {
	Val int
}
type EEmpty struct{}

func (e EVal) Error() string {
	return fmt.Sprintf("EVal:%d", e.Val)
}
func (EEmpty) Error() string { return "EEmpty" }

func TestIncompleted(t *testing.T) {
	assert.Panics(t, func() {
		errors.NewMap(errors.Default())
	})
	assert.Panics(t, func() {
		errors.NewMap(errors.F(func(error) bool { return false }))
	})
	assert.Panics(t, func() {
		errors.NewMap(errors.Tp(EVal{}))
	})
	assert.Panics(t, func() {
		errors.NewMap(errors.Eq(EVal{}))
	})
}

func TestMapping(t *testing.T) {
	var mapping = errors.NewMap(
		errors.F(func(e error) bool {
			return e.Error() == "EVal:0"
		}).V(stderr.New("eval-f")),
		errors.Eq(EVal{Val: 2}).V(stderr.New("eval-val")),
		errors.Tp(EVal{}).V(stderr.New("eval-tp")),
		errors.Default().V(stderr.New("default")),
	)
	cases := []struct {
		in  error
		out string
	}{
		{in: EVal{Val: 0}, out: "eval-f"},
		{in: EVal{Val: 1}, out: "eval-tp"},
		{in: EVal{Val: 2}, out: "eval-val"},
		{in: EEmpty{}, out: "default"},
	}
	for _, c := range cases {
		assert.Equal(t, c.out, mapping.Map(c.in).Error())
	}

	assert.Equal(
		t,
		[]error{
			stderr.New("eval-f"),
			stderr.New("eval-val"),
			stderr.New("eval-tp"),
			stderr.New("default"),
		},
		mapping.Returns(),
	)
}

func TestMakerFunc(t *testing.T) {
	mapping := errors.NewMap(
		errors.Default().F(func(e error) error {
			return stderr.New(e.Error() + "1")
		}),
	)
	assert.Equal(t, "EEmpty1", mapping.Map(EEmpty{}).Error())
	assert.Empty(t, mapping.Returns())

	mapping = errors.NewMap(
		errors.Default().F(func(e error) error {
			return stderr.New(e.Error() + "1")
		}).Returns(EEmpty{}, EVal{}),
	)
	assert.Equal(t, "EEmpty1", mapping.Map(EEmpty{}).Error())
	assert.Equal(
		t,
		[]error{EEmpty{}, EVal{}},
		mapping.Returns(),
	)
}

func TestMakerNil(t *testing.T) {
	var mapping = errors.NewMap(errors.Default().Nil())
	assert.Nil(t, mapping.Map(EEmpty{}))
	assert.Empty(t, mapping.Returns())
}

func TestNotMapped(t *testing.T) {
	var mapping = errors.NewMap(errors.Tp(EVal{}).Nil())
	assert.ErrorIs(t, mapping.Map(EEmpty{}), EEmpty{})
	assert.Empty(t, mapping.Returns())
}
