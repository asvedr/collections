package errors

import (
	stderr "errors"
	"reflect"
)

type Map interface {
	Map(error) error
	Returns() []error
}

type _Map struct {
	mappers []Mapper
}

type Mapper struct {
	matcher func(error) bool
	maker   func(error) error
	returns []error
}

func NewMap(mappers ...Mapper) Map {
	for _, m := range mappers {
		if m.matcher == nil || m.maker == nil {
			panic("incompleted mapper")
		}
	}
	return _Map{mappers: mappers}
}

func (m _Map) Map(err error) error {
	if err == nil {
		return nil
	}
	for _, mapper := range m.mappers {
		if mapper.matcher(err) {
			return mapper.maker(err)
		}
	}
	return err
}

func (m _Map) Returns() []error {
	keys := map[string]bool{}
	var res []error
	for _, mapper := range m.mappers {
		for _, err := range mapper.returns {
			key := err.Error()
			if keys[key] {
				continue
			}
			keys[key] = true
			res = append(res, err)
		}
	}
	return res
}

func Eq(err error) Mapper {
	f := func(e error) bool { return stderr.Is(e, err) }
	return Mapper{matcher: f}
}

func Tp(err error) Mapper {
	exp := reflect.TypeOf(err)
	f := func(e error) bool {
		return reflect.TypeOf(e) == exp
	}
	return Mapper{matcher: f}
}

func F(f func(error) bool) Mapper {
	return Mapper{matcher: f}
}

func always_true(error) bool {
	return true
}

func Default() Mapper {
	return Mapper{matcher: always_true}
}

func (m Mapper) F(f func(error) error) Mapper {
	m.maker = f
	return m
}

func (m Mapper) Returns(err ...error) Mapper {
	m.returns = err
	return m
}

func (m Mapper) V(err error) Mapper {
	m.maker = func(error) error { return err }
	m.returns = []error{err}
	return m
}

func ret_nil(error) error { return nil }

func (m Mapper) Nil() Mapper {
	m.maker = ret_nil
	return m
}
