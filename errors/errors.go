package errors

func SeqExec(funcs ...func() error) error {
	var err error
	for _, f := range funcs {
		if err = f(); err != nil {
			return err
		}
	}
	return nil
}

func SeqExecVar(
	err_var *error,
	funcs ...func(),
) error {
	for _, f := range funcs {
		f()
		if *err_var != nil {
			return *err_var
		}
	}
	return nil
}
