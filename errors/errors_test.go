package errors_test

import (
	stderr "errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/errors"
)

func TestSeqExec(t *testing.T) {
	var out []int
	err := errors.SeqExec(
		func() error {
			out = append(out, 1)
			return stderr.New("err")
		},
		func() error {
			out = append(out, 2)
			return nil
		},
	)
	assert.Equal(t, "err", err.Error())
	assert.Equal(t, []int{1}, out)

	out = []int{}
	err = errors.SeqExec(
		func() error {
			out = append(out, 1)
			return nil
		},
		func() error {
			out = append(out, 2)
			return nil
		},
	)
	assert.NoError(t, err)
	assert.Equal(t, []int{1, 2}, out)
}

func TestSeqExecVar(t *testing.T) {
	var out []int
	var err error
	err = errors.SeqExecVar(
		&err,
		func() {
			out = append(out, 1)
			err = stderr.New("err")
		},
		func() {
			out = append(out, 2)
		},
	)
	assert.Equal(t, "err", err.Error())
	assert.Equal(t, []int{1}, out)

	out = []int{}
	err = nil
	err = errors.SeqExecVar(
		&err,
		func() { out = append(out, 1) },
		func() { out = append(out, 2) },
	)
	assert.NoError(t, err)
	assert.Equal(t, []int{1, 2}, out)
}
