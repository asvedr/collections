package maps_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/maps"
)

func TestChunkifyEmpty(t *testing.T) {
	assert.Equal(
		t,
		[]map[string]int{},
		maps.Chunkify(map[string]int{}, 2),
	)
	count := 0
	maps.ChunkifyIter(
		func(m map[string]int) error {
			count += 1
			return nil
		},
		map[string]int{},
		2,
	)
	assert.Equal(t, 0, count)
}

func TestChunkify6x4(t *testing.T) {
	src := map[string]int{
		"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6,
	}

	chunks := maps.Chunkify(src, 4)
	assert.Equal(t, 2, len(chunks))
	assert.Equal(t, 4, len(chunks[0]))
	assert.Equal(t, 2, len(chunks[1]))
	processed := map[string]int{}
	for _, chunk := range chunks {
		for k, v := range chunk {
			processed[k] = v
		}
	}
	assert.Equal(t, src, processed)

	processed = map[string]int{}
	maps.ChunkifyIter(
		func(chunk map[string]int) error {
			assert.True(t, len(chunk) == 2 || len(chunk) == 4)
			for k, v := range chunk {
				processed[k] = v
			}
			return nil
		},
		src,
		4,
	)
	assert.Equal(t, src, processed)
}

func TestChunkify3x6(t *testing.T) {
	src := map[string]int{"a": 1, "b": 2, "c": 3}
	chunks := maps.Chunkify(src, 6)
	assert.Equal(t, 1, len(chunks))
	assert.Equal(t, src, chunks[0])
}

func TestFromSlice(t *testing.T) {
	res := maps.FromSlice(
		func(word string) (int, string) {
			return len(word), word
		},
		[]string{
			"hello",
			"alex",
			"world",
		},
	)
	exp := map[int]string{4: "alex", 5: "world"}
	assert.Equal(t, exp, res)
}
