package maps

func Chunkify[K comparable, V any](
	dict map[K]V,
	chunk_size int,
) []map[K]V {
	res := []map[K]V{}
	chunk := make(map[K]V, chunk_size)
	for key, val := range dict {
		chunk[key] = val
		if len(chunk) == chunk_size {
			res = append(res, chunk)
			chunk = make(map[K]V, chunk_size)
		}
	}
	if len(chunk) > 0 {
		res = append(res, chunk)
	}
	return res
}

func ChunkifyIter[K comparable, V any](
	f func(map[K]V) error,
	dict map[K]V,
	chunk_size int,
) error {
	chunk := make(map[K]V, chunk_size)
	var err error
	for key, val := range dict {
		chunk[key] = val
		if len(chunk) == chunk_size {
			err = f(chunk)
			if err != nil {
				return err
			}
			chunk = make(map[K]V, chunk_size)
		}
	}
	if len(chunk) > 0 {
		err = f(chunk)
	}
	return err
}

func FromSlice[K comparable, T, V any](
	f func(T) (K, V),
	src []T,
) map[K]V {
	res := map[K]V{}
	for _, v := range src {
		key, val := f(v)
		res[key] = val
	}
	return res
}
