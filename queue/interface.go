package queue

type Queue[T any] interface {
	Push(t T) bool
	Peek() (T, bool)
	Pop() (T, bool)
	Len() int
}
