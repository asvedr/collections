package queue_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/queue"
)

func TestHeapPeekNull(t *testing.T) {
	stack := queue.NewHeap[string]()
	assert.Equal(t, 0, stack.Len())
	_, got := stack.Peek()
	assert.False(t, got)
	_, got = stack.Pop()
	assert.False(t, got)
}

func TestHeapPushPeekPeekPop(t *testing.T) {
	stack := queue.NewHeap[string]()
	stack.Push("a")
	val, got := stack.Peek()
	assert.True(t, got)
	assert.Equal(t, "a", val)
	val, got = stack.Peek()
	assert.True(t, got)
	assert.Equal(t, "a", val)
	stack.Pop()
	_, got = stack.Peek()
	assert.False(t, got)
}

func TestHeapPushPushPopPush(t *testing.T) {
	stack := queue.NewHeap[string]()
	stack.Push("1")
	stack.Push("2")
	stack.Pop()
	stack.Push("3")

	as_vec := []string{}
	for stack.Len() > 0 {
		val, got := stack.Pop()
		assert.True(t, got)
		as_vec = append(as_vec, val)
	}
	assert.Equal(t, []string{"2", "3"}, as_vec)
}
