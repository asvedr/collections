package queue

type QueueHeap[T any] struct {
	pop_tail  *heap_node[T]
	push_tail *heap_node[T]
	len       int
}

var _ Queue[any] = (*QueueHeap[any])(nil)

type heap_node[T any] struct {
	val  T
	next *heap_node[T]
}

func NewHeap[T any]() *QueueHeap[T] {
	return &QueueHeap[T]{}
}

func (self *QueueHeap[T]) Push(val T) bool {
	node := &heap_node[T]{val: val}
	if self.len == 0 {
		self.pop_tail = node
	} else {
		self.push_tail.next = node
	}
	self.push_tail = node
	self.len += 1
	return true
}

func (self *QueueHeap[T]) Peek() (T, bool) {
	if self.len == 0 {
		var res T
		return res, false
	}
	return self.pop_tail.val, true
}

func (self *QueueHeap[T]) Pop() (T, bool) {
	var res T
	if self.len == 0 {
		return res, false
	}
	res = self.pop_tail.val
	self.pop_tail = self.pop_tail.next
	self.len -= 1
	return res, true
}

func (self *QueueHeap[T]) Len() int {
	return self.len
}
