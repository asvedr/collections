package queue

type QueueSlice[T any] struct {
	buffer  []T
	head    int
	tail    int
	max_len int
	elems   int
}

var _ Queue[int] = (*QueueSlice[int])(nil)

func NewSlice[T any](max_len int) *QueueSlice[T] {
	return &QueueSlice[T]{
		buffer:  make([]T, max_len),
		head:    0,
		tail:    0,
		max_len: max_len,
	}
}

func (self *QueueSlice[T]) Push(t T) bool {
	if self.elems == self.max_len {
		return false
	}
	self.buffer[self.head] = t
	self.head = (self.head + 1) % self.max_len
	self.elems += 1
	return true
}

func (self *QueueSlice[T]) Peek() (T, bool) {
	if self.elems == 0 {
		var t T
		return t, false
	}
	return self.buffer[self.tail], true
}

func (self *QueueSlice[T]) Pop() (T, bool) {
	if self.elems == 0 {
		var t T
		return t, false
	}
	elem := self.buffer[self.tail]
	self.tail = (self.tail + 1) % self.max_len
	self.elems -= 1
	return elem, true
}

func (self *QueueSlice[T]) Len() int {
	return self.elems
}
