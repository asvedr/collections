package queue_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/queue"
	"gitlab.com/asvedr/collections/tuple"
)

func TestSliceQueueEmpty(t *testing.T) {
	q := queue.NewSlice[int](5)
	assert.Equal(t, 0, q.Len())
	assert.False(t, tuple.Snd(q.Peek()))
	assert.False(t, tuple.Snd(q.Pop()))
	assert.Equal(t, 0, q.Len())
}

func TestSliceQueueNoOverlap(t *testing.T) {
	q := queue.NewSlice[int](5)
	assert.True(t, q.Push(1))
	assert.True(t, q.Push(2))
	assert.Equal(
		t,
		tuple.NewPair(1, true),
		tuple.NewPair(q.Pop()),
	)
	assert.True(t, q.Push(3))
	assert.True(t, q.Push(4))
	assert.Equal(
		t,
		tuple.NewPair(2, true),
		tuple.NewPair(q.Pop()),
	)
	assert.Equal(
		t,
		[]int{3, 4},
		to_slice(q),
	)
	assert.Equal(t, 0, q.Len())
}

func TestSliceQueueOverlap(t *testing.T) {
	q := queue.NewSlice[int](2)
	assert.True(t, q.Push(1))
	assert.True(t, q.Push(2))
	assert.False(t, q.Push(3))

	assert.Equal(t, []int{1, 2}, to_slice(q))
}

func TestPeek(t *testing.T) {
	q := queue.NewSlice[int](5)
	assert.True(t, q.Push(1))
	assert.True(t, q.Push(2))
	assert.Equal(
		t,
		tuple.NewPair(1, true),
		tuple.NewPair(q.Peek()),
	)
}

func to_slice[T any](q queue.Queue[T]) []T {
	res := []T{}
	for q.Len() > 0 {
		elt, ok := q.Pop()
		if !ok {
			panic("unreachable")
		}
		res = append(res, elt)
	}
	return res
}
