package cons_test

import (
	"fmt"
	"iter"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/cons"
)

func s123() cons.List[int] {
	return cons.FromSlice([]int{1, 2, 3})
}

func TestToSlice(t *testing.T) {
	assert.Equal(t, []int{1, 2, 3}, s123().Slice())
}

func TestHeadTail(t *testing.T) {
	list := s123()
	val, ok := list.Head()
	assert.Equal(t, 1, val)
	assert.True(t, ok)

	list = list.Tail().Tail()
	val, ok = list.Head()
	assert.Equal(t, 3, val)
	assert.True(t, ok)

	list = list.Tail()
	_, ok = list.Head()
	assert.False(t, ok)

	assert.Equal(t, list, list.Tail())
}

func TestRev(t *testing.T) {
	assert.Equal(t, []int{3, 2, 1}, s123().Rev().Slice())
}

func TestMap(t *testing.T) {
	assert.Equal(
		t,
		[]string{"2", "3", "4"},
		cons.Map(
			func(x int) string {
				return fmt.Sprint(x + 1)
			},
			s123(),
		).Slice(),
	)
}

func TestFilterMap(t *testing.T) {
	assert.Equal(
		t,
		[]string{"1", "3"},
		cons.FilterMap(
			func(x int) (string, bool) {
				if x%2 == 1 {
					return fmt.Sprint(x), true
				}
				return "", false
			},
			s123(),
		).Slice(),
	)
}

func TestFilter(t *testing.T) {
	assert.Equal(
		t,
		[]int{1, 3},
		s123().Filter(
			func(x int) bool {
				return x%2 == 1
			},
		).Slice(),
	)
}

func TestIter(t *testing.T) {
	var _ iter.Seq[int] = s123().Iter
	slice := []int{}
	for x := range s123().Iter {
		slice = append(slice, x)
	}
	assert.Equal(t, []int{1, 2, 3}, slice)
}
