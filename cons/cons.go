package cons

type List[T any] struct {
	node *node[T]
}

type node[T any] struct {
	head   T
	tail   *node[T]
	length int
}

func Cons[T any](head T, tail List[T]) List[T] {
	return List[T]{
		node: &node[T]{
			head:   head,
			tail:   tail.node,
			length: 1 + tail.Len(),
		},
	}
}

func (self List[T]) Head() (T, bool) {
	if self.node == nil {
		var zero T
		return zero, false
	}
	return self.node.head, true
}

func (self List[T]) Tail() List[T] {
	if self.node == nil {
		return self
	}
	return List[T]{node: self.node.tail}
}

func (self List[T]) IsNil() bool {
	return self.node == nil
}

func (self List[T]) Len() int {
	if self.node == nil {
		return 0
	}
	return self.node.length
}

func FromSlice[S ~[]T, T any](src S) List[T] {
	var res List[T]
	for i := len(src) - 1; i >= 0; i-- {
		res = Cons(src[i], res)
	}
	return res
}

func (self List[T]) Slice() []T {
	res := make([]T, self.Len())
	i := 0
	for !self.IsNil() {
		item, _ := self.Head()
		res[i] = item
		self = self.Tail()
		i += 1
	}
	return res
}

func (self List[T]) Rev() List[T] {
	var res List[T]
	for !self.IsNil() {
		item, _ := self.Head()
		res = Cons(item, res)
		self = self.Tail()
	}
	return res
}

func (self List[T]) Iter(yield func(arg T) bool) {
	for !self.IsNil() {
		item, _ := self.Head()
		if !yield(item) {
			return
		}
		self = self.Tail()
	}
}

func (self List[T]) Filter(f func(T) bool) List[T] {
	if self.IsNil() {
		return self
	}
	item, _ := self.Head()
	tail := self.Tail().Filter(f)
	if f(item) {
		return Cons(item, tail)
	}
	return tail
}

func Map[A, B any](f func(A) B, list List[A]) List[B] {
	if list.IsNil() {
		return List[B]{}
	}
	item, _ := list.Head()
	head := f(item)
	tail := Map(f, list.Tail())
	return Cons(head, tail)
}

func FilterMap[A, B any](f func(A) (B, bool), list List[A]) List[B] {
	if list.IsNil() {
		return List[B]{}
	}
	item, _ := list.Head()
	head, save := f(item)
	tail := FilterMap(f, list.Tail())
	if save {
		return Cons(head, tail)
	}
	return tail
}
