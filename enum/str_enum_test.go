package enum_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/enum"
)

type MyEnum int

const (
	A MyEnum = iota
	C
	B
)

var e = enum.NewStr(map[MyEnum]string{
	A: "A", B: "B", C: "C",
})

func TestAll(t *testing.T) {
	assert.Equal(t, []MyEnum{A, C, B}, e.All())
	assert.Equal(t, []string{"A", "C", "B"}, e.Keys())
}

func TestToStr(t *testing.T) {
	val, ok := e.ToStr(B)
	assert.True(t, ok)
	assert.Equal(t, "B", val)

	_, ok = e.ToStr(MyEnum(10))
	assert.False(t, ok)
}

func TestFromStr(t *testing.T) {
	val, ok := e.FromStr("C")
	assert.True(t, ok)
	assert.Equal(t, C, val)

	_, ok = e.FromStr("abc")
	assert.False(t, ok)
}
