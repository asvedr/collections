package enum

import (
	"cmp"
	"slices"
)

type Str[T cmp.Ordered] struct {
	to_str   map[T]string
	from_str map[string]T
	all      []T
}

func NewStr[T cmp.Ordered](to_str map[T]string) Str[T] {
	from_str := make(map[string]T, len(to_str))
	all := []T{}
	for key, val := range to_str {
		from_str[val] = key
		all = append(all, key)
	}
	slices.Sort(all)
	return Str[T]{
		to_str:   to_str,
		from_str: from_str,
		all:      all,
	}
}

func (self Str[T]) All() []T {
	return self.all
}

func (self Str[T]) Keys() []string {
	res := make([]string, len(self.to_str))
	for i, val := range self.all {
		res[i] = self.to_str[val]
	}
	return res
}

func (self Str[T]) ToStr(t T) (string, bool) {
	val, ok := self.to_str[t]
	return val, ok
}

func (self Str[T]) FromStr(s string) (T, bool) {
	val, ok := self.from_str[s]
	return val, ok
}
