package sets_test

import (
	stdslices "slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/sets"
)

func TestFromSlice(t *testing.T) {
	assert.Equal(
		t,
		map[int]bool{1: true, 2: true, 3: true},
		sets.FromSlice([]int{1, 2, 3}),
	)
}

func TestMapKeys(t *testing.T) {
	assert.Equal(
		t,
		map[int]bool{1: true, 2: true, 3: true},
		sets.MapKeys(map[int]bool{1: true, 2: true, 3: true}),
	)
}

func TestMapValues(t *testing.T) {
	assert.Equal(
		t,
		map[rune]bool{'a': true, 'b': true, 'c': true},
		sets.MapValues(map[int]rune{1: 'a', 2: 'b', 3: 'c'}),
	)
}

func TestCross(t *testing.T) {
	res := sets.Cross(
		map[int]string{1: "a", 2: "b", 3: "c"},
		map[int]bool{2: true, 3: true, 4: true},
	)
	assert.Equal(t, map[int]string{2: "b", 3: "c"}, res)

	res = sets.Cross(
		map[int]string{},
		map[int]bool{2: true, 3: true, 4: true},
	)
	assert.Equal(t, map[int]string{}, res)

	res = sets.Cross(
		map[int]string{1: "a", 2: "b", 3: "c"},
		map[int]bool{},
	)
	assert.Equal(t, map[int]string{}, res)
}

func TestSub(t *testing.T) {
	res := sets.Sub(
		map[int]rune{1: 'a', 2: 'b', 3: 'c'},
		map[int]bool{2: true, 3: true, 4: true},
	)
	assert.Equal(t, map[int]rune{1: 'a'}, res)
}

func TestUnion(t *testing.T) {
	res := sets.Union(
		map[int]rune{1: 'a', 2: 'b', 3: 'c'},
		map[int]rune{2: 'a', 3: 'b', 4: 'c'},
	)
	assert.Equal(
		t,
		map[int]rune{1: 'a', 2: 'b', 3: 'c', 4: 'c'},
		res,
	)
}

func TestCollect(t *testing.T) {
	iter := stdslices.Values([]int{1, 2, 3})
	assert.Equal(
		t,
		map[int]bool{1: true, 2: true, 3: true},
		sets.Collect(iter),
	)
}
