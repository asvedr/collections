package sets

import "iter"

func FromSlice[T comparable](src []T) map[T]bool {
	res := map[T]bool{}
	for _, v := range src {
		res[v] = true
	}
	return res
}

func MapKeys[K comparable, V any](src map[K]V) map[K]bool {
	res := map[K]bool{}
	for k := range src {
		res[k] = true
	}
	return res
}

func MapValues[K, V comparable](src map[K]V) map[V]bool {
	res := map[V]bool{}
	for _, k := range src {
		res[k] = true
	}
	return res
}

func Cross[K comparable, A, B any](
	set_a map[K]A,
	set_b map[K]B,
) map[K]A {
	res := map[K]A{}
	for key, val := range set_a {
		_, found := set_b[key]
		if found {
			res[key] = val
		}
	}
	return res
}

func Sub[K comparable, A, B any](
	set_a map[K]A,
	set_b map[K]B,
) map[K]A {
	res := map[K]A{}
	for key, val := range set_a {
		_, found := set_b[key]
		if !found {
			res[key] = val
		}
	}
	return res
}

func Union[K comparable, T any](
	set_a map[K]T,
	set_b map[K]T,
) map[K]T {
	res := map[K]T{}
	for k, v := range set_a {
		res[k] = v
	}
	for k, v := range set_b {
		_, found := set_a[k]
		if !found {
			res[k] = v
		}
	}
	return res
}

func Collect[T comparable](src iter.Seq[T]) map[T]bool {
	res := map[T]bool{}
	for item := range src {
		res[item] = true
	}
	return res
}
