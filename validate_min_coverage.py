import sys
from typing import Iterable


FILE = "coverage.html"
TARGET_COVERAGE = 95.0


def extract_num(src: Iterable[str]) -> Iterable[float]:
    for line in src:
        if '<option value="file' in line:
            num = line.split(" (")[1].split("%)")[0]
            yield float(num)


def main():
    with open(FILE) as handler:
        cov_list = list(extract_num(handler))
    min_coverage = min(cov_list)
    if min_coverage < TARGET_COVERAGE:
        print(f"FAILED: {min_coverage} < {TARGET_COVERAGE}")
        sys.exit(1)
    print(f"OK: {min_coverage} >= {TARGET_COVERAGE}")
    avg = sum(cov_list) / len(cov_list)
    print(f"AVG(coverage): {avg}")

main()
