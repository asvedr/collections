package opt

func Default[T any](
	ptr *T,
	dflt T,
) T {
	if ptr == nil {
		return dflt
	}
	return *ptr
}

func FromValue[T any](t T) *T {
	return &t
}
