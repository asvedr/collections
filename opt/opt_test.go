package opt_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/opt"
)

func TestDefault(t *testing.T) {
	var x, y *int
	x = opt.FromValue(1)
	assert.Equal(t, 1, opt.Default(x, 123))
	assert.Equal(t, 123, opt.Default(y, 123))
}
