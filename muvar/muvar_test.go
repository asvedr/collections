package muvar_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/muvar"
)

func TestGetSet(t *testing.T) {
	v := muvar.New(123)
	assert.Equal(t, 123, v.Get())
	v.Set(321)
	assert.Equal(t, 321, v.Get())
}

func TestWith(t *testing.T) {
	v := muvar.New(123)
	v.With(func(x int) int { return x * 2 })
	assert.Equal(t, 246, v.Get())
}
