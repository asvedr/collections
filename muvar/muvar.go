package muvar

import "sync"

type Var[T any] struct {
	mu   sync.Mutex
	data T
}

func New[T any](data T) *Var[T] {
	return &Var[T]{data: data}
}

func (v *Var[T]) Get() T {
	v.mu.Lock()
	defer v.mu.Unlock()
	return v.data
}

func (v *Var[T]) Set(data T) {
	v.mu.Lock()
	defer v.mu.Unlock()
	v.data = data
}

func (v *Var[T]) With(f func(T) T) {
	v.mu.Lock()
	defer v.mu.Unlock()
	v.data = f(v.data)
}
