package tuple

type Pair[A, B any] struct {
	Fst A
	Snd B
}

type Triple[A, B, C any] struct {
	Fst  A
	Snd  B
	Thrd C
}

func Fst[A, B any](a A, b B) A {
	return a
}

func Snd[A, B any](a A, b B) B {
	return b
}

func NewPair[A, B any](a A, b B) Pair[A, B] {
	return Pair[A, B]{Fst: a, Snd: b}
}
