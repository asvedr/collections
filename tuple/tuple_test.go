package tuple_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/tuple"
)

func TestFstSnd(t *testing.T) {
	assert.Equal(t, 1, tuple.Fst(1, 2))
	assert.Equal(t, 2, tuple.Snd(1, 2))
}

func TestNewPair(t *testing.T) {
	assert.Equal(
		t,
		tuple.Pair[int, int]{Fst: 1, Snd: 2},
		tuple.NewPair(1, 2),
	)
}
