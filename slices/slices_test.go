package slices_test

import (
	"errors"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/slices"
)

func TestMapKeys(t *testing.T) {
	keys := slices.MapKeys(map[int]string{
		1: "a", 2: "b", 3: "c",
	})
	sort.Ints(keys)
	assert.Equal(t, []int{1, 2, 3}, keys)
}

func TestMapValues(t *testing.T) {
	vals := slices.MapValues(map[int]string{
		1: "a", 2: "b", 3: "c",
	})
	sort.Strings(vals)
	assert.Equal(t, []string{"a", "b", "c"}, vals)
}

func TestChunkify(t *testing.T) {
	assert.Equal(
		t,
		[][]int{},
		slices.Chunkify([]int{}, 2),
	)
	assert.Equal(
		t,
		[][]int{{1, 2}, {3, 4}, {5, 6}},
		slices.Chunkify([]int{1, 2, 3, 4, 5, 6}, 2),
	)
	assert.Equal(
		t,
		[][]int{{1, 2, 3, 4}, {5, 6}},
		slices.Chunkify([]int{1, 2, 3, 4, 5, 6}, 4),
	)
	assert.Equal(
		t,
		[][]int{{1, 2, 3}},
		slices.Chunkify([]int{1, 2, 3}, 5),
	)
}

func TestMap(t *testing.T) {
	assert.Equal(
		t,
		[]int{2, 3, 4},
		slices.Map(
			func(x int) int { return x + 1 },
			[]int{1, 2, 3},
		),
	)
}

func TestMapErr(t *testing.T) {
	e := errors.New("custom")
	f := func(x int) (int, error) {
		if x > 3 {
			return 0, e
		} else {
			return x + 1, nil
		}
	}
	res, err := slices.MapErr(f, []int{1, 2, 3})
	assert.NoError(t, err)
	assert.Equal(t, []int{2, 3, 4}, res)
	_, err = slices.MapErr(f, []int{1, 5, 2})
	assert.Equal(t, e, err)
}
