package slices

func MapKeys[K comparable, V any](src map[K]V) []K {
	res := make([]K, len(src))
	i := 0
	for k := range src {
		res[i] = k
		i += 1
	}
	return res
}

func MapValues[K comparable, V any](src map[K]V) []V {
	res := make([]V, len(src))
	i := 0
	for _, v := range src {
		res[i] = v
		i += 1
	}
	return res
}

func Chunkify[T any](
	list []T,
	chunk_size int,
) [][]T {
	res := [][]T{}
	list_len := len(list)
	for begin := 0; begin < list_len; begin += chunk_size {
		end := min(begin+chunk_size, list_len)
		chunk := list[begin:end]
		if len(chunk) > 0 {
			res = append(res, chunk)
		}
	}
	return res
}

func Map[A, B any](f func(A) B, slice []A) []B {
	res := make([]B, len(slice))
	for i, v := range slice {
		res[i] = f(v)
	}
	return res
}

func MapErr[A, B any](f func(A) (B, error), slice []A) ([]B, error) {
	res := make([]B, len(slice))
	var err error
	for i, v := range slice {
		res[i], err = f(v)
		if err != nil {
			break
		}
	}
	return res, err
}
