package avl

import (
	"cmp"
	"fmt"
	"strings"
)

type node[K cmp.Ordered, V any] struct {
	key    K
	value  V
	left   *node[K, V]
	right  *node[K, V]
	height int
	length int
}

func map_node_to_string[K cmp.Ordered, V any](node *node[K, V], cnt int) string {
	prefix := strings.Repeat("  ", cnt)
	if node == nil {
		return prefix + "nil\n"
	}
	text := fmt.Sprintf("%s%v: %v\n", prefix, node.key, node.value)
	text += map_node_to_string(node.left, cnt+1)
	text += map_node_to_string(node.right, cnt+1)
	return text
}

func set_node_to_string[K cmp.Ordered](node *node[K, struct{}], cnt int) string {
	prefix := strings.Repeat("  ", cnt)
	if node == nil {
		return prefix + "nil\n"
	}
	text := fmt.Sprintf("%s%v\n", prefix, node.key)
	text += set_node_to_string(node.left, cnt+1)
	text += set_node_to_string(node.right, cnt+1)
	return text
}

func rec_get[K cmp.Ordered, V any](root *node[K, V], key K) (V, bool) {
	if root == nil {
		var zero V
		return zero, false
	}
	if key == root.key {
		return root.value, true
	}
	if key < root.key {
		return rec_get(root.left, key)
	}
	return rec_get(root.right, key)
}

func rec_has[K cmp.Ordered](root *node[K, struct{}], key K) bool {
	if root == nil {
		return false
	}
	if key == root.key {
		return true
	}
	if key < root.key {
		return rec_has(root.left, key)
	}
	return rec_has(root.right, key)
}

func rec_insert[K cmp.Ordered, V any](
	root *node[K, V],
	key K,
	val V,
	replace_same bool,
) (*node[K, V], bool) {
	if root == nil {
		res := node[K, V]{key: key, value: val, height: 1, length: 1}
		return &res, true
	}
	if key == root.key {
		if !replace_same {
			return root, false
		}
		res := &node[K, V]{
			key:    key,
			value:  val,
			left:   root.left,
			right:  root.right,
			height: root.height,
			length: root.length,
		}
		return res, true
	}
	var left, right *node[K, V]
	var changed bool
	if key < root.key {
		left, changed = rec_insert(
			root.left, key, val, replace_same,
		)
		right = root.right
	} else {
		left = root.left
		right, changed = rec_insert(
			root.right, key, val, replace_same,
		)
	}
	if !changed {
		return root, false
	}
	res := build_balanced_node(root.key, root.value, left, right)
	return res, true
}

func rec_delete[K cmp.Ordered, V any](
	root *node[K, V],
	key K,
) (*node[K, V], bool) {
	if root == nil {
		return nil, false
	}
	if key == root.key {
		return delete_node(root), true
	}
	var left, right *node[K, V]
	var changed bool
	if key < root.key {
		left, changed = rec_delete(root.left, key)
		right = root.right
	} else {
		right, changed = rec_delete(root.right, key)
		left = root.left
	}
	if !changed {
		return root, false
	}
	res := build_balanced_node(root.key, root.value, left, right)
	return res, true
}

func delete_node[K cmp.Ordered, V any](root *node[K, V]) *node[K, V] {
	if root.left == nil {
		return root.right
	}
	if root.right == nil {
		return root.left
	}
	new_root := root.right
	for key, val := range iterator(root.left) {
		new_root, _ = rec_insert(new_root, key, val, false)
	}
	return new_root
}

func build_balanced_node[K cmp.Ordered, V any](
	key K,
	val V,
	left *node[K, V],
	right *node[K, V],
) (res *node[K, V]) {
	var t turner[K, V]
	root := build_node(key, val, left, right)
	switch t.get_type(root) {
	case turn_type_none:
		return root
	case turn_type_left_simple:
		return t.turn_left_simple(root)
	case turn_type_left_complex:
		return t.turn_left_complex(root)
	case turn_type_right_simple:
		return t.turn_right_simple(root)
	case turn_type_right_complex:
		return t.turn_right_complex(root)
	default:
		panic("unreachable")
	}
}

func (self *node[K, V]) copy(left, right *node[K, V]) *node[K, V] {
	return build_node(self.key, self.value, left, right)
}

func build_node[K cmp.Ordered, V any](
	key K,
	val V,
	left *node[K, V],
	right *node[K, V],
) *node[K, V] {
	return &node[K, V]{
		key:    key,
		value:  val,
		left:   left,
		right:  right,
		height: max(get_height(left), get_height(right)) + 1,
		length: get_length(left) + get_length(right) + 1,
	}
}

func get_height[K cmp.Ordered, V any](root *node[K, V]) int {
	if root == nil {
		return 0
	}
	return root.height
}

func get_child_heights[K cmp.Ordered, V any](root *node[K, V]) (int, int) {
	return get_height(root.left), get_height(root.right)
}

func get_length[K cmp.Ordered, V any](root *node[K, V]) int {
	if root == nil {
		return 0
	}
	return root.length
}
