package avl_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/avl"
)

func TestHeightEmpty(t *testing.T) {
	assert.Equal(t, 0, empty_tree.Height())
}

func TestBalancing(t *testing.T) {
	tree := avl.Map[int, int]{}
	max_key, max_height := 1000, 10
	for i := range max_key {
		tree = tree.Add(i, i)
	}
	tree_height := tree.Height()
	assert.True(t, tree_height <= max_height)

	for i := range max_key {
		key := fmt.Sprint(i)
		t.Run(key, func(t *testing.T) {
			sub := tree.Remove(i)
			height := sub.Height()
			assert.True(
				t,
				height <= tree_height+1,
				"height: %d > %d",
				height,
				max_height,
			)
		})
	}
}
