package avl_test

import (
	"fmt"
	"slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/avl"
)

var empty_set avl.Set[string]

func TestImmutableAddHas(t *testing.T) {
	tree2 := empty_set.Add("a").Add("b")
	tree4 := tree2.Add("c").Add("d").Add("b")

	assert.Equal(t, 0, empty_set.Height())
	assert.Equal(t, 0, empty_set.Length())
	assert.False(t, empty_set.Has("a"))
	assert.False(t, empty_set.Has("b"))

	assert.Equal(t, 2, tree2.Length())

	assert.True(t, tree2.Has("a"))
	assert.True(t, tree2.Has("b"))
	assert.False(t, tree2.Has("c"))
	assert.False(t, tree2.Has("d"))

	assert.Equal(t, 4, tree4.Length())

	assert.True(t, tree4.Has("a"))
	assert.True(t, tree4.Has("b"))
	assert.True(t, tree4.Has("c"))
	assert.True(t, tree4.Has("d"))
}

func TestImmutableRemoveSet(t *testing.T) {
	elems := "abcdefghij"
	tree := empty_set
	for _, sym := range elems {
		tree = tree.Add(string(sym))
	}
	assert.Equal(t, 10, tree.Length())
	assert.Equal(t, 4, tree.Height())

	test_body := func(t *testing.T, key string) {
		sub_tree := tree.Remove(key)
		assert.Equal(t, 9, sub_tree.Length())
		assert.Equal(t, 10, tree.Length())

		for _, rsym := range elems {
			sym := string(rsym)
			assert.Equal(t, sym != key, sub_tree.Has(sym))
			assert.True(t, tree.Has(sym))
		}
	}

	for _, sym := range elems {
		key := string(sym)
		t.Run(key, func(t *testing.T) {
			test_body(t, key)
		})
	}
}

func TestKeysSet(t *testing.T) {
	tree := empty_set.Add("b").Add("a").Add("c")
	assert.Equal(
		t,
		slices.Collect(tree.GetKeys),
		[]string{"a", "b", "c"},
	)
}

func TestIterSet(t *testing.T) {
	tree := empty_set.Add("b").Add("a").Add("c")
	assert.Equal(
		t,
		slices.Collect(tree.Iter),
		[]string{"a", "b", "c"},
	)
}

var str_set = `TreeSet:
  b
    a
      nil
      nil
    c
      nil
      d
        nil
        nil
`

func TestSetToString(t *testing.T) {
	tree := empty_set.Add("a").Add("b").Add("c").Add("d")
	assert.Equal(t, str_set, fmt.Sprint(tree))
}

var str_balanced_set = `TreeSet:
  c
    a
      nil
      b
        nil
        nil
    e
      d
        nil
        nil
      nil
`

var set_without_a = `TreeSet:
  c
    b
      nil
      nil
    e
      d
        nil
        nil
      nil
`

var set_without_e = `TreeSet:
  c
    a
      nil
      b
        nil
        nil
    d
      nil
      nil
`

func TestDeleteSetCornerCases(t *testing.T) {
	tree := empty_set.Add("c").Add("a")
	tree = tree.Add("e").Add("d").Add("b")
	assert.Equal(t, str_balanced_set, fmt.Sprint(tree))

	assert.Equal(
		t,
		set_without_a,
		fmt.Sprint(tree.Remove("a")),
	)

	assert.Equal(
		t,
		set_without_e,
		fmt.Sprint(tree.Remove("e")),
	)

	assert.Equal(
		t,
		str_balanced_set,
		fmt.Sprint(tree.Remove("x")),
	)
}
