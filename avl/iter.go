package avl

import (
	"cmp"
	"iter"
)

func iterator[K cmp.Ordered, V any](root *node[K, V]) iter.Seq2[K, V] {
	return func(yield func(K, V) bool) {
		iterate_map(root, yield)
	}
}

func iterate_map[K cmp.Ordered, V any](
	root *node[K, V],
	yield func(K, V) bool,
) bool {
	if root == nil {
		return true
	}
	if !iterate_map(root.left, yield) {
		return false
	}
	if !yield(root.key, root.value) {
		return false
	}
	return iterate_map(root.right, yield)
}

func iterate_set[K cmp.Ordered, V any](
	root *node[K, V],
	yield func(K) bool,
) bool {
	if root == nil {
		return true
	}
	if !iterate_set(root.left, yield) {
		return false
	}
	if !yield(root.key) {
		return false
	}
	return iterate_set(root.right, yield)
}
