package avl

import (
	"cmp"
)

type Set[K cmp.Ordered] struct {
	root *node[K, struct{}]
}

func (self Set[K]) String() string {
	return "TreeSet:\n" + set_node_to_string(self.root, 1)
}

func (self Set[K]) Length() int {
	if self.root == nil {
		return 0
	}
	return self.root.length
}

func (self Set[K]) Height() int {
	if self.root == nil {
		return 0
	}
	return self.root.height
}

func (self Set[K]) Has(key K) bool {
	return rec_has(self.root, key)
}

func (self Set[K]) Add(key K) Set[K] {
	var val struct{}
	root, _ := rec_insert(self.root, key, val, true)
	return Set[K]{root}
}

func (self Set[K]) Remove(key K) Set[K] {
	root, _ := rec_delete(self.root, key)
	return Set[K]{root}
}

func (self Set[K]) GetKeys(yield func(K) bool) {
	iterate_set(self.root, yield)
}

func (self Set[K]) Iter(yield func(K) bool) {
	iterate_set(self.root, yield)
}
