package avl

import "cmp"

type turn_type int

const (
	turn_type_none turn_type = iota
	turn_type_left_simple
	turn_type_left_complex
	turn_type_right_simple
	turn_type_right_complex
)

type turner[K cmp.Ordered, V any] struct{}

func (turner[K, V]) get_type(root *node[K, V]) turn_type {
	lh, rh := get_child_heights(root)
	if lh > rh+1 {
		llh, lrh := get_child_heights(root.left)
		if lrh > llh {
			return turn_type_right_complex
		}
		return turn_type_right_simple
	} else if rh > lh+1 {
		rlh, rrh := get_child_heights(root.right)
		if rlh > rrh {
			return turn_type_left_complex
		}
		return turn_type_left_simple
	}
	return turn_type_none
}

func (turner[K, V]) turn_right_simple(root *node[K, V]) *node[K, V] {
	left := root.left.left
	right := root.copy(root.left.right, root.right)
	return root.left.copy(left, right)
}

func (turner[K, V]) turn_right_complex(root *node[K, V]) *node[K, V] {
	new_root := root.left.right
	left := root.left.copy(root.left.left, new_root.left)
	right := root.copy(new_root.right, root.right)
	return new_root.copy(left, right)
}

func (turner[K, V]) turn_left_simple(root *node[K, V]) *node[K, V] {
	left := root.copy(root.left, root.right.left)
	right := root.right.right
	return root.right.copy(left, right)
}

func (turner[K, V]) turn_left_complex(root *node[K, V]) *node[K, V] {
	new_root := root.right.left
	left := root.copy(root.left, new_root.right)
	right := root.right.copy(new_root.left, root.right.right)
	return new_root.copy(left, right)
}
