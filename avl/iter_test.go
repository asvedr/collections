package avl_test

import (
	"slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/avl"
	"gitlab.com/asvedr/collections/iters"
)

func TestIter(t *testing.T) {
	var tree avl.Map[string, int]
	tree = tree.Add("a", 10).Add("b", 20).Add("c", 30)
	var keys []string
	var vals []int
	for key, val := range tree.Iter {
		keys = append(keys, key)
		vals = append(vals, val)
	}
	assert.Equal(t, []string{"a", "b", "c"}, keys)
	assert.Equal(t, []int{10, 20, 30}, vals)
}

func TestIterEmpty(t *testing.T) {
	var tree avl.Map[string, int]
	keys := slices.Collect(iters.Keys(tree.Iter))
	assert.Equal(t, 0, len(keys))
}

func TestIterMapBreakOnLeft(t *testing.T) {
	var tree avl.Map[string, int]
	tree = tree.Add("g", 1).Add("c", 1).Add("m", 1)
	tree = tree.Add("a", 1).Add("b", 1)
	tree = tree.Add("l", 1).Add("z", 1)
	tree.Iter(func(k string, _ int) bool {
		return k != "a"
	})
}

func TestIterSetBreakOnLeft(t *testing.T) {
	var tree avl.Set[string]
	tree = tree.Add("g").Add("c").Add("m")
	tree = tree.Add("a").Add("b")
	tree = tree.Add("l").Add("z")
	tree.Iter(func(k string) bool {
		return k != "a"
	})
}
