package avl_test

import (
	"fmt"
	"slices"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/avl"
)

var empty_tree avl.Map[string, int]

func TestImmutableAddGet(t *testing.T) {
	tree2 := empty_tree.Add("a", 4).Add("b", 5)
	tree4 := tree2.Add("c", 6).Add("d", 7).Add("b", 8)

	assert.Equal(t, 0, empty_tree.Length())
	_, ok := empty_tree.Get("a")
	assert.False(t, ok)
	_, ok = empty_tree.Get("b")
	assert.False(t, ok)

	assert.Equal(t, 2, tree2.Length())

	elt, ok := tree2.Get("a")
	assert.True(t, ok)
	assert.Equal(t, 4, elt)
	elt, ok = tree2.Get("b")
	assert.True(t, ok)
	assert.Equal(t, 5, elt)
	_, ok = tree2.Get("c")
	assert.False(t, ok)
	_, ok = tree2.Get("d")
	assert.False(t, ok)

	assert.Equal(t, 4, tree4.Length())

	elt, ok = tree4.Get("a")
	assert.True(t, ok)
	assert.Equal(t, 4, elt)
	elt, ok = tree4.Get("b")
	assert.True(t, ok)
	assert.Equal(t, 8, elt)
	elt, ok = tree4.Get("c")
	assert.True(t, ok)
	assert.Equal(t, 6, elt)
	elt, ok = tree4.Get("d")
	assert.True(t, ok)
	assert.Equal(t, 7, elt)
}

func TestImmutableRemove(t *testing.T) {
	elems := "abcdefghij"
	tree := empty_tree
	for i, sym := range elems {
		tree = tree.Add(string(sym), i)
	}
	assert.Equal(t, 10, tree.Length())
	assert.Equal(t, 4, tree.Height())

	test_body := func(t *testing.T, key string) {
		sub_tree := tree.Remove(key)
		assert.Equal(t, 9, sub_tree.Length())
		assert.Equal(t, 10, tree.Length())

		for _, rsym := range elems {
			sym := string(rsym)
			_, ok := sub_tree.Get(sym)
			assert.Equal(t, sym != key, ok)
			_, ok = tree.Get(sym)
			assert.True(t, ok)
		}
	}

	for _, sym := range elems {
		key := string(sym)
		t.Run(key, func(t *testing.T) {
			test_body(t, key)
		})
	}
}

func TestKeys(t *testing.T) {
	tree := empty_tree.Add("b", 1).Add("a", 2).Add("c", 3)
	assert.Equal(
		t,
		slices.Collect(tree.GetKeys),
		[]string{"a", "b", "c"},
	)
}

var str_tree = `TreeMap:
  b: 2
    a: 1
      nil
      nil
    c: 3
      nil
      d: 4
        nil
        nil
`

func TestToString(t *testing.T) {
	tree := empty_tree.Add("a", 1).Add("b", 2).Add("c", 3).Add("d", 4)
	assert.Equal(t, str_tree, fmt.Sprint(tree))
}

var str_balanced_tree = `TreeMap:
  c: 1
    a: 1
      nil
      b: 1
        nil
        nil
    e: 1
      d: 1
        nil
        nil
      nil
`

var without_a = `TreeMap:
  c: 1
    b: 1
      nil
      nil
    e: 1
      d: 1
        nil
        nil
      nil
`

var without_e = `TreeMap:
  c: 1
    a: 1
      nil
      b: 1
        nil
        nil
    d: 1
      nil
      nil
`

func TestDeleteCornerCases(t *testing.T) {
	tree := empty_tree.Add("c", 1).Add("a", 1)
	tree = tree.Add("e", 1).Add("d", 1).Add("b", 1)
	assert.Equal(t, str_balanced_tree, fmt.Sprint(tree))

	assert.Equal(
		t,
		without_a,
		fmt.Sprint(tree.Remove("a")),
	)

	assert.Equal(
		t,
		without_e,
		fmt.Sprint(tree.Remove("e")),
	)

	assert.Equal(
		t,
		str_balanced_tree,
		fmt.Sprint(tree.Remove("x")),
	)
}
