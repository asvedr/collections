package avl

import (
	"cmp"
)

type Map[K cmp.Ordered, V any] struct {
	root *node[K, V]
}

func (self Map[K, V]) String() string {
	return "TreeMap:\n" + map_node_to_string(self.root, 1)
}

func (self Map[K, V]) Length() int {
	if self.root == nil {
		return 0
	}
	return self.root.length
}

func (self Map[K, V]) Height() int {
	if self.root == nil {
		return 0
	}
	return self.root.height
}

func (self Map[K, V]) Get(key K) (V, bool) {
	return rec_get(self.root, key)
}

func (self Map[K, V]) Add(key K, value V) Map[K, V] {
	root, _ := rec_insert(self.root, key, value, true)
	return Map[K, V]{root}
}

func (self Map[K, V]) Remove(key K) Map[K, V] {
	root, _ := rec_delete(self.root, key)
	return Map[K, V]{root}
}

func (self Map[K, V]) GetKeys(yield func(K) bool) {
	iterate_set(self.root, yield)
}

func (self Map[K, V]) Iter(yield func(K, V) bool) {
	iterate_map(self.root, yield)
}
