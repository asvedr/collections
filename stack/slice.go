package stack

type StackSlice[T any] struct {
	buffer  []T
	end     int
	max_end int
}

var _ Stack[any] = (*StackSlice[any])(nil)

func NewSlice[T any](max_len int) *StackSlice[T] {
	return &StackSlice[T]{
		buffer:  make([]T, max_len),
		end:     -1,
		max_end: max_len - 1,
	}
}

func (self *StackSlice[T]) Push(t T) bool {
	if self.end == self.max_end {
		return false
	}
	self.end += 1
	self.buffer[self.end] = t
	return true
}

func (self *StackSlice[T]) Peek() (T, bool) {
	var item T
	if self.end < 0 {
		return item, false
	}
	item = self.buffer[self.end]
	return item, true
}

func (self *StackSlice[T]) Pop() (T, bool) {
	var item T
	if self.end >= 0 {
		item = self.buffer[self.end]
		self.end -= 1
		return item, true
	}
	return item, false
}

func (self *StackSlice[T]) Len() int {
	return self.end + 1
}
