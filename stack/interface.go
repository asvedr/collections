package stack

type Stack[T any] interface {
	Push(t T) bool
	Peek() (T, bool)
	Pop() (T, bool)
	Len() int
}
