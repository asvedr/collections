package stack

type StackHeap[T any] struct {
	top    *node[T]
	length int
}

var _ Stack[any] = (*StackHeap[any])(nil)

type node[T any] struct {
	value T
	prev  *node[T]
}

func NewHeap[T any]() *StackHeap[T] {
	return &StackHeap[T]{nil, 0}
}

func (self *StackHeap[T]) Push(value T) bool {
	n := &node[T]{value, self.top}
	self.top = n
	self.length += 1
	return true
}

func (self *StackHeap[T]) Pop() (T, bool) {
	if self.length == 0 {
		var zero T
		return zero, false
	}
	value := self.top.value
	self.top = self.top.prev
	self.length -= 1
	return value, true
}

func (self *StackHeap[T]) Peek() (T, bool) {
	if self.length == 0 {
		var zero T
		return zero, false
	}
	return self.top.value, true
}

func (self *StackHeap[T]) Len() int {
	return self.length
}
