package stack_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/stack"
)

func TestVecPeekNull(t *testing.T) {
	stack := stack.NewSlice[string](10)
	assert.Equal(t, 0, stack.Len())
	_, got := stack.Peek()
	assert.False(t, got)
}

func TestVecPushPeekPeekPop(t *testing.T) {
	stack := stack.NewSlice[string](10)
	stack.Push("a")
	val, got := stack.Peek()
	assert.True(t, got)
	assert.Equal(t, "a", val)
	val, got = stack.Peek()
	assert.True(t, got)
	assert.Equal(t, "a", val)
	stack.Pop()
	_, got = stack.Peek()
	assert.False(t, got)
}

func TestVecPushPushPopPush(t *testing.T) {
	stack := stack.NewSlice[string](10)
	stack.Push("1")
	stack.Push("2")
	stack.Pop()
	stack.Push("3")

	as_vec := []string{}
	for stack.Len() > 0 {
		val, got := stack.Pop()
		assert.True(t, got)
		as_vec = append(as_vec, val)
	}
	assert.Equal(t, []string{"3", "1"}, as_vec)
}

func TestVecOverflow(t *testing.T) {
	stack := stack.NewSlice[int](5)
	for i := 1; i < 10; i += 1 {
		stack.Push(i)
	}
	pushed := stack.Push(11)
	assert.False(t, pushed)
	as_vec := []int{}
	for stack.Len() > 0 {
		val, got := stack.Pop()
		assert.True(t, got)
		as_vec = append(as_vec, val)
	}
	assert.Equal(t, []int{5, 4, 3, 2, 1}, as_vec)
}

func TestVecPopEmpty(t *testing.T) {
	stack := stack.NewSlice[int](5)
	_, got := stack.Pop()
	assert.False(t, got)
}
