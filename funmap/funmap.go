package funmap

import "reflect"

type Map[T any] struct {
	funs map[reflect.Type]func(any) (T, error)
}

func Add[Arg, Res any](funs Map[Res], f func(Arg) Res) Map[Res] {
	if funs.funs == nil {
		funs.funs = make(
			map[reflect.Type]func(any) (Res, error),
		)
	}
	wrapped := func(arg any) (Res, error) {
		return f(arg.(Arg)), nil
	}
	var t Arg
	funs.funs[reflect.TypeOf(t)] = wrapped
	return funs
}

func AddErr[Arg, Res any](
	funs Map[Res],
	f func(Arg) (Res, error),
) Map[Res] {
	if funs.funs == nil {
		funs.funs = make(
			map[reflect.Type]func(any) (Res, error),
		)
	}
	wrapped := func(arg any) (Res, error) {
		return f(arg.(Arg))
	}
	var t Arg
	funs.funs[reflect.TypeOf(t)] = wrapped
	return funs
}

func (self Map[T]) Get(t any) func(any) (T, error) {
	if self.funs == nil {
		return nil
	}
	return self.funs[reflect.TypeOf(t)]
}
