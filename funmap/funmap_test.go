package funmap_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/collections/funmap"
	"gitlab.com/asvedr/collections/tuple"
)

func hi(x string) string {
	return fmt.Sprintf("hi %s", x)
}

func add(x int) string {
	return fmt.Sprintf("%d", x+1)
}

func TestNil(t *testing.T) {
	assert.Nil(t, funmap.Map[string]{}.Get(1))
}

func TestMap(t *testing.T) {
	m := funmap.Add(
		funmap.Add(funmap.Map[string]{}, add),
		hi,
	)

	res, err := m.Get("a")("a")
	assert.Equal(t, "hi a", res)
	assert.NoError(t, err)

	res, err = m.Get(1)(1)
	assert.Equal(t, "2", res)
	assert.NoError(t, err)

	assert.Nil(t, m.Get('x'))
}

func TestMapErr(t *testing.T) {
	m := funmap.AddErr(
		funmap.Map[int]{},
		func(x int) (int, error) {
			if x < 0 {
				return 0, errors.New("err")
			}
			return x + 1, nil
		},
	)
	f := m.Get(1)
	assert.Error(t, tuple.Snd(f(-1)))
	res, err := f(2)
	assert.Equal(t, 3, res)
	assert.NoError(t, err)
}
